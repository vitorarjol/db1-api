<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
// $app->get('/', function () use ($app) {
//     return $app->version();
// });
$app->group(['prefix' => 'api/v1'], function ($app) {

    $app->post('users','UsersController@store');
    $app->get('users/{id}','UsersController@show');
    $app->put('users/{id}','UsersController@update');

    $app->post('login','LoginController@store');

    $app->group(['middleware' => 'auth'], function ($app) {
        $app->delete('logout','LoginController@destroy');
        $app->post('posts','PostsController@store');
        $app->put('posts/{id}','PostsController@update');
        $app->delete('posts/{id}','PostsController@destroy');
    });

    $app->get('posts','PostsController@index');
    $app->get('posts/{id}','PostsController@show');

    $app->get('tags','TagsController@index');
    $app->post('tags','TagsController@store');
    $app->get('tags/{id}','TagsController@show');
    $app->put('tags/{id}','TagsController@update');
    $app->delete('tags/{id}','TagsController@destroy');

    $app->get('categories','CategoriesController@index');
    $app->post('categories','CategoriesController@store');
    $app->get('categories/{id}','CategoriesController@show');
    $app->put('categories/{id}','CategoriesController@update');
    $app->delete('categories/{id}','CategoriesController@destroy');
});
