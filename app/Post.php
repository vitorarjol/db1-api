<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
        'title', 'summary', 'description', 'publishing_date', 'category_id', 'user_id'
    ];

    public function author()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    public function getPublishingDateAttribute($value)
    {
        return date('d/m/Y', strtotime($value));
    }

    public function setPublishingDateAttribute($value)
    {
        if (!$value) {
            $this->attributes['publishing_date'] = date('Y-m-d');
        } else {
            $this->attributes['publishing_date'] = date('Y-m-d', strtotime(str_replace('/', '-', $value)));
        }
    }

}
