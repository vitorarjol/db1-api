<?php

namespace App\Http\Transformers;

class PostsTransformer extends Transformer
{
    private $authorTransformer;
    private $categoriesTransformer;
    private $tagsTransformer;

    public function __construct(CategoriesTransformer $categoriesTransformer, TagsTransformer $tagsTransformer, UsersTransformer $authorTransformer)
    {
        $this->categoriesTransformer = $categoriesTransformer;
        $this->tagsTransformer = $tagsTransformer;
        $this->authorTransformer = $authorTransformer;
    }

    public function transform($post)
    {
        return [
            'id'  =>  $post['id'],
            'title' => $post['title'],
            'summary' => $post['summary'],
            'description' => $post['description'],
            'publishing_date' => $post['publishing_date'],
            'tags' => $post['tags'] ? $this->tagsTransformer->transformCollection($post['tags']) : [],
            'category' => $this->categoriesTransformer->transform($post['category']),
            'author' => $this->authorTransformer->transform($post['author']),
        ];
    }
}
