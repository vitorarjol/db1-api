<?php

namespace App\Http\Transformers;

class CategoriesTransformer extends Transformer
{
    public function transform($category)
    {
        return [
            'id'  =>  $category['id'],
            'name' => $category['name'],
        ];
    }
}
