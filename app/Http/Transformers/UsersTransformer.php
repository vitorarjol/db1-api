<?php

namespace App\Http\Transformers;

class UsersTransformer extends Transformer
{
    public function transform($user)
    {
        return [
            'id'  =>  $user['id'],
            'name' => $user['name'],
            'email' => $user['email'],
        ];
    }
}
