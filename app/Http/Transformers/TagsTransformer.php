<?php

namespace App\Http\Transformers;

class TagsTransformer extends Transformer
{
    public function transform($tag)
    {
        return [
            'id'  =>  $tag['id'],
            'name' => $tag['name'],
        ];
    }
}
