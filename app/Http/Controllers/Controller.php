<?php

namespace App\Http\Controllers;

use App\Http\Traits\ApiUtilities;
use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use ApiUtilities;

    protected $errorMessages = [];
    protected $storeRules = [];
    protected $updateRules = [];
    protected $transformer;
}
