<?php

namespace App\Http\Controllers;

use App\Events\PostCreated;
use App\Http\Transformers\PostsTransformer;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class PostsController extends Controller
{
    public function __construct(PostsTransformer $transformer)
    {
        $this->transformer = $transformer;

        $this->storeRules = [
            "title" => "required",
            "description" => "required",
            "category_id" => "required",
        ];
        $this->updateRules = $this->storeRules;
        $this->errorMessages = [
            "title.required" => "O título do post é obrigatório.",
            "description.required" => "A descrição do post é obrigatória.",
            "category_id.required" => "A categoria do post é obrigatória.",
        ];
    }

    public function index()
    {
        $posts = $this->loadFromCache();

        return $this->respond([
            "data" => $this->transformer->transformCollection($posts->toArray())
        ]);
    }


    public function store(Request $request)
    {
        $this->validate($request, $this->storeRules, $this->errorMessages);

        $post = Post::create([
            "title" => $request->input("title"),
            "description" => $request->input("description"),
            "summary" => $request->input("summary") ?? null,
            "publishing_date" => $request->input("publishing_date") ?? null,
            "category_id" => $request->input("category_id"),
            "user_id" => app("auth")->user()->id,
        ]);

        $post->tags()->sync($request->input("tags"));

        if (!$post) {
            return $this->respondWithError("Falha ao adicionar o post.");
        }

        event(new PostCreated($this->transformer->transform($post->load("author", "category", "tags")->toArray())));

        $this->clearCache();

        return $this->respondCreated("Post adicionado com sucesso.");
    }


    public function show($postId)
    {
        if (! $post = Post::with(["author", "category", "tags"])->find($postId)) {
            return $this->respondNotFound("Não foi possível encontrar um post com o identificador fornecido.");
        }

        return $this->respond([
            "data" => $this->transformer->transform($post->toArray())
        ]);
    }


    public function update(Request $request, $postId)
    {
        if (! $post = Post::find($postId)) {
            return $this->respondNotFound("Não foi possível encontrar um post com o identificador fornecido.");
        }

        if (!$request->user()->can("update-post", $post)) {
            return $this->setStatusCode(403)->respondWithError("Você não possui permissão para atualizar esse post.");
        }

        $this->validate($request, $this->updateRules, $this->errorMessages);

        $post->title = $request->input("title");
        $post->description = $request->input("description");
        $post->summary = $request->input("summary") ?? null;
        $post->publishing_date = $request->input("publishing_date") ?? null;
        $post->category_id = $request->input("category_id");

        if (!$post->save() || !$post->tags()->sync($request->input("tags"))) {
            return $this->respondWithError("Falha ao atualizar o post.");
        }

        $this->clearCache();

        return $this->respond([ "message" => "Post atualizado com sucesso."]);
    }


    public function destroy(Request $request, $postId)
    {
        if (! $post = Post::find($postId)) {
            return $this->respondNotFound("Não foi possível encontrar um post com o identificador fornecido.");
        }

        if (!$request->user()->can("update-post", $post)) {
            return $this->setStatusCode(403)->respondWithError("Você não possui permissão para remover esse post.");
        }

        if (!$post->tags()->sync([]) || !$post->delete()) {
            return $this->respondWithError("Falha ao remover o post.");
        }

        $this->clearCache();

        return $this->respond([ "message" => "Post removido com sucesso." ]);
    }

    protected function loadFromCache()
    {
        return Cache::remember("posts.all", 60 * 60 * 24, function () {
            return Post::with(["author", "category", "tags"])
            ->where("publishing_date", "<=", date("Y-m-d"))
            ->orderBy("id", "desc")
            ->get();
        });
    }

    protected function clearCache()
    {
        Cache::forget("posts.all");
    }
}
