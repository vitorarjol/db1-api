<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function __construct()
    {
        $this->storeRules = [
            "email" => "required|email",
            "password" => "required",
        ];
        $this->errorMessages = [
            "email.required" => "O e-mail é obrigatório.",
            "email.email"  => "Informe um e-mail válido.",
            "password.required" => "A senha é obrigatória.",
        ];
    }


    public function store(Request $request)
    {
        $this->validate($request, $this->storeRules, $this->errorMessages);

        $user = User::where("email",$request->input("email"))->first();

        if (!$user || !app('hash')->check($request->input("password"), $user->password)) {
            return $this->setStatusCode(422)->respond([
                "message" => "O e-mail e senha digitados não conferem. Revise suas credenciais e tente novamente."
            ]);
        }

        $user->update([
            "remember_token" => base64_encode(str_random(40))
        ]);

        return $this->respond([
            "message" => "Usuário conectado com sucesso.",
            "access_token" => $user->remember_token,
            "user_id" => $user->id,
        ]);
    }

    public function destroy()
    {
        $user = User::find(app('auth')->user()->id);

        if (!$user->update(["remember_token" => null])) {
            return $this->respondWithError("Falha ao desconectar o usuário.");
        }

        return $this->respond([ "message" => "Usuário desconectado com sucesso."]);
    }
}
