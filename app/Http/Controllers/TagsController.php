<?php

namespace App\Http\Controllers;

use App\Http\Transformers\TagsTransformer;
use App\Tag;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class TagsController extends Controller
{
    public function __construct(TagsTransformer $transformer)
    {
        $this->transformer = $transformer;

        $this->storeRules = [
            "name" => "required|unique:tags",
        ];
        $this->errorMessages = [
            "name.required" => "O nome da tag é obrigatório.",
            "name.unique"  => "Já existe uma tag cadastrada com esse nome.",
        ];
    }

    public function index()
    {
        $tags = Tag::all();

        return $this->respond([
            "data" => $this->transformer->transformCollection($tags->toArray())
        ]);
    }


    public function store(Request $request)
    {
        $this->validate($request, $this->storeRules, $this->errorMessages);

        if (!Tag::create($request->only("name"))) {
            return $this->respondWithError("Falha ao adicionar a tag.");
        }

        return $this->respondCreated("Tag adicionada com sucesso.");
    }


    public function show($tagId)
    {
        if (! $tag = Tag::find($tagId)) {
            return $this->respondNotFound("Não foi possível encontrar uma tag com o identificador fornecido.");
        }

        return $this->respond([
            "data" => $this->transformer->transform($tag->toArray())
        ]);
    }


    public function update(Request $request, $tagId)
    {
        if (! $tag = Tag::find($tagId)) {
            return $this->respondNotFound("Não foi possível encontrar uma tag com o identificador fornecido.");
        }

        $this->updateRules = [
            "name" => [
                "required",
                Rule::unique("tags")->ignore($tag->id)
            ]
        ];

        $this->validate($request, $this->updateRules, $this->errorMessages);

        $tag->name = $request->input("name");

        if (!$tag->save()) {
            return $this->respondWithError("Falha ao atualizar a tag.");
        }

        return $this->respond([ "message" => "Tag atualizada com sucesso."]);
    }


    public function destroy($tagId)
    {
        if (! $tag = Tag::find($tagId)) {
            return $this->respondNotFound("Não foi possível encontrar uma tag com o identificador fornecido.");
        }

        if (!$tag->delete()) {
            return $this->respondWithError("Falha ao remover a tag.");
        }

        return $this->respond([ 'message' => "Tag removida com sucesso." ]);
    }
}
