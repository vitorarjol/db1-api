<?php

namespace App\Http\Controllers;

use App\Http\Transformers\CategoriesTransformer;
use App\Category;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class CategoriesController extends Controller
{
    public function __construct(CategoriesTransformer $transformer)
    {
        $this->transformer = $transformer;

        $this->storeRules = [
            "name" => "required|unique:categories",
        ];
        $this->errorMessages = [
            "name.required" => "O nome da categoria é obrigatório.",
            "name.unique"  => "Já existe uma categoria cadastrada com esse nome.",
        ];
    }

    public function index()
    {
        $categories = Category::all();

        return $this->respond([
            "data" => $this->transformer->transformCollection($categories->toArray())
        ]);
    }

    public function store(Request $request)
    {
        $this->validate($request, $this->storeRules, $this->errorMessages);

        if (!Category::create($request->only("name"))) {
            return $this->respondWithError("Falha ao adicionar a categoria.");
        }

        return $this->respondCreated("Categoria adicionada com sucesso.");
    }


    public function show($categoryId)
    {
        if (! $category = Category::find($categoryId)) {
            return $this->respondNotFound("Não foi possível encontrar uma categoria com o identificador fornecido.");
        }

        return $this->respond([
            "data" => $this->transformer->transform($category->toArray())
        ]);
    }


    public function update(Request $request, $categoryId)
    {
        if (! $category = Category::find($categoryId)) {
            return $this->respondNotFound("Não foi possível encontrar uma categoria com o identificador fornecido.");
        }

        $this->updateRules = [
            "name" => [
                "required",
                Rule::unique("categories")->ignore($category->id)
            ]
        ];

        $this->validate($request, $this->updateRules, $this->errorMessages);

        $category->name = $request->input("name");

        if (!$category->save()) {
            return $this->respondWithError("Falha ao atualizar a categoria.");
        }

        return $this->respond([ "message" => "Categoria atualizada com sucesso."]);
    }

    public function destroy($categoryId)
    {
        if (! $category = Category::find($categoryId)) {
            return $this->respondNotFound("Não foi possível encontrar uma categoria com o identificador fornecido.");
        }

        if (!$category->delete()) {
            return $this->respondWithError("Falha ao remover a categoria.");
        }

        return $this->respond([ 'message' => "Categoria removida com sucesso." ]);
    }
}
