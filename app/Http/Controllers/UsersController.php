<?php

namespace App\Http\Controllers;

use App\Http\Transformers\UsersTransformer;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class UsersController extends Controller
{
    public function __construct(UsersTransformer $transformer)
    {
        $this->transformer = $transformer;

        $this->storeRules = [
            "name" => "required",
            "email" => "required|email|unique:users",
            "password" => "required",
            'password_confirmation' => 'required|same:password',
        ];
        $this->errorMessages = [
            "name.required" => "O nome do usuário é obrigatório.",
            "email.required" => "O e-mail é obrigatório.",
            "email.unique"  => "Esse e-mail já está em uso.",
            "email.email"  => "Informe um e-mail válido.",
            "password.required" => "A senha é obrigatória.",
            'password_confirmation.required' => 'A confirmação de senha é de preenchimento obrigatório.',
            'password_confirmation.same' => 'As senhas digitadas devem ser iguais.',
        ];
    }

    public function index()
    {
        $users = User::all();

        return $this->respond([
            "data" => $this->transformer->transformCollection($users->toArray())
        ]);
    }


    public function store(Request $request)
    {
        $this->validate($request, $this->storeRules, $this->errorMessages);

        if (!User::create($request->only("name", "email", "password"))) {
            return $this->respondWithError("Falha ao adicionar o usuário.");
        }

        return $this->respondCreated("Usuário adicionado com sucesso.");
    }


    public function show($userId)
    {
        if (! $user = User::find($userId)) {
            return $this->respondNotFound("Não foi possível encontrar um usuário com o identificador fornecido.");
        }

        return $this->respond([
            "data" => $this->transformer->transform($user->toArray())
        ]);
    }


    public function update(Request $request, $userId)
    {
        if (! $user = User::find($userId)) {
            return $this->respondNotFound("Não foi possível encontrar uma tag com o identificador fornecido.");
        }

        $this->updateRules = [
            "name" => "required",
            "email" => [
                "required",
                "email",
                Rule::unique("users")->ignore($user->id),
            ],
            "password" => "required",
            'password_confirmation' => 'required|same:password',
        ];

        $this->validate($request, $this->updateRules, $this->errorMessages);

        $user->name = $request->input("name");
        $user->email = $request->input("email");
        $user->password = $request->input("password");

        if (!$user->save()) {
            return $this->respondWithError("Falha ao atualizar o usuário.");
        }

        return $this->respond([ "message" => "Usuário atualizado com sucesso."]);
    }
}
