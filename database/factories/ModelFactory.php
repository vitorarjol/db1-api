<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        "name" => $faker->name,
        "email" => $faker->unique()->safeEmail,
        "password" => "secret",
        "remember_token" => str_random(10),
    ];
});

$factory->define(App\Tag::class, function (Faker\Generator $faker) {
    return [
        "name" => $faker->word,
    ];
});

$factory->define(App\Category::class, function (Faker\Generator $faker) {
    return [
        "name" => $faker->word,
    ];
});

$factory->define(App\Post::class, function (Faker\Generator $faker) {
    return [
        "title" => $faker->sentence,
        "summary" => $faker->sentence,
        "description" => $faker->paragraphs(3, true),
        "publishing_date" => date('d/m/Y'),
        "category_id" => function () {
            return factory(App\Category::class)->create()->id;
        },
        "user_id" => function () {
            return factory(App\User::class)->create()->id;
        }
    ];
});
