<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tags = factory(App\Tag::class, 10)->create();
        factory(App\Post::class, 20)->create()->each(function($post) use ($tags) {
            $post->tags()->saveMany($tags->shuffle()->take(2)->all());
        });
    }
}
