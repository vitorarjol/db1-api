<?php

use App\User;
use Laravel\Lumen\Testing\DatabaseTransactions;

class LoginTest extends TestCase
{
    use DatabaseTransactions;
    protected $resourceUrl = "login";

    /** @test */
    public function it_shows_a_error_message_for_wrong_credentials()
    {
        $user = factory(User::class)->create();
        $this
            ->json("POST", $this->resourceUrl, [
                "email" => $user->email,
                "password" => "wrong-credentials",
            ])
            ->seeJson([
                "message" => "O e-mail e senha digitados não conferem. Revise suas credenciais e tente novamente."
            ])
            ->assertResponseStatus(422);
    }

    /** @test */
    public function it_creates_a_new_acess_token_for_a_user_given_valid_credentials()
    {
        $user = factory(User::class)->create();

        $this
            ->json("POST", $this->resourceUrl, [
                "email" => $user->email,
                "password" => "secret",
            ])
            ->seeJson([
                "message" => "Usuário conectado com sucesso.",
            ])
            ->assertResponseStatus(200);
    }

    /** @test */
    public function it_throws_a_422_if_a_new_login_request_fails_validation()
    {
        $testUser = factory(User::class)->create();

        $this
            ->json("POST", $this->resourceUrl, [
                "email" => "",
                "password" => "",
            ])
            ->seeJson([
                "email" => [
                    "O e-mail é obrigatório.",
                ],
                "password" => [
                    "A senha é obrigatória.",
                ],
            ])
            ->assertResponseStatus(422);
    }

}
