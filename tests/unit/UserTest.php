<?php

use App\User;
use Laravel\Lumen\Testing\DatabaseTransactions;

class UserTest extends TestCase
{
    use DatabaseTransactions;
    protected $resourceUrl = "users";

    /** @test */
    public function it_fetches_a_single_user()
    {
        $user = factory(User::class)->create();

        $this
            ->json("GET", $this->resourceUrl . "/" . $user->id)
            ->seeJson([
                "name" => $user->name,
                "email" => $user->email,
            ])
            ->assertResponseStatus(200);
    }

    /** @test */
    public function it_throws_a_404_if_a_user_is_not_found()
    {
        $this
            ->json("GET", $this->resourceUrl . "/999999")
            ->seeJson([
                "error" => [
                    "message" => "Não foi possível encontrar um usuário com o identificador fornecido.",
                    "status_code" => 404
                ]
            ])
            ->assertResponseStatus(404);
    }

    /** @test */
    public function it_creates_a_new_user_given_valid_parameters()
    {
        $this
            ->json("POST", $this->resourceUrl, [
                "name" => "Vítor Arjol",
                "email" => "vitor.arjol@db1.com.br",
                "password" => "db1_testes",
                "password_confirmation" => "db1_testes",
            ])
            ->seeJson([
                "message" => "Usuário adicionado com sucesso."
            ])
            ->assertResponseStatus(201);

        $this->seeInDatabase("users", [
            "name" => "Vítor Arjol",
            "email" => "vitor.arjol@db1.com.br",
        ]);
    }

    /** @test */
    public function it_throws_a_422_if_a_new_user_request_fails_validation()
    {
        $testUser = factory(User::class)->create();

        $this
            ->json("POST", $this->resourceUrl, [
                "name" => "Vítor Arjol",
                "email" => "vitor.arjol@db1.com.br",
                "password" => "db1_testes",
            ])
            ->seeJson([
                "password_confirmation" => ["A confirmação de senha é de preenchimento obrigatório."]
            ])
            ->assertResponseStatus(422);
    }

    /** @test */
    public function it_updates_a_user_given_valid_parameters()
    {
        $user = factory(User::class)->create();

        $this
            ->json("PUT", $this->resourceUrl . "/" . $user->id, [
                "name" => "Vítor",
                "email" => "testes@db1.com.br",
                "password" => "db1_testes",
                "password_confirmation" => "db1_testes",
            ])
            ->seeJson([
                "message" => "Usuário atualizado com sucesso."
            ])
            ->assertResponseStatus(200);

        $this->seeInDatabase("users", [
            "name" => "Vítor",
            "email" => "testes@db1.com.br",
        ]);
    }


    /** @test */
    public function it_throws_a_422_if_a_updating_user_request_fails_validation()
    {
        $user = factory(User::class)->create();

        $this
            ->json("PUT", $this->resourceUrl . "/" . $user->id, ['name' => ''])
            ->seeJson([
                "email" => [
                    "O e-mail é obrigatório.",
                ],
                "name" => [
                    "O nome do usuário é obrigatório.",
                ],
                "password" => [
                    "A senha é obrigatória.",
                ],
                'password_confirmation' => [
                    'A confirmação de senha é de preenchimento obrigatório.',
                ],
            ])
            ->assertResponseStatus(422);
    }
}
