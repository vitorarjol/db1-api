<?php

use App\Category;
use App\Post;
use App\Tag;
use App\User;
use Laravel\Lumen\Testing\DatabaseTransactions;

class PostTest extends TestCase
{
    use DatabaseTransactions;

    protected $faker;
    protected $fakeTitle;
    protected $fakeSummary;
    protected $resourceUrl = "posts";
    protected $user;

    function setUp()
    {
        parent::setUp();
        $this->faker = Faker\Factory::create();

        $this->fakeTitle = $this->faker->sentence;
        $this->fakeSummary = $this->faker->sentence;

        $this->user = factory(User::class)->create();
    }

    /** @test */
    public function it_fetches_all_posts()
    {
        $tags = factory(Tag::class, 10)->create();
        $posts = factory(Post::class, 5)->create()->each(function($post) use ($tags) {
            $post->tags()->saveMany($tags->shuffle()->take(3)->all());
        });

        $randomPost = $posts[rand(0, 4)];

        $this
            ->json("GET", $this->resourceUrl)
            ->seeJson([
                "title" => $randomPost->title,
                "summary" => $randomPost->summary,
                "description" => $randomPost->description,
                "category" => [
                    "id" => $randomPost->category->id,
                    "name" => $randomPost->category->name,
                ],
                "author" => [
                    "id" => $randomPost->author->id,
                    "name" => $randomPost->author->name,
                    "email" => $randomPost->author->email,
                ],
            ])
            ->assertResponseStatus(200);
    }

    /** @test */
    public function it_fetches_a_single_post()
    {
        $post = factory(Post::class)->create();

        $this
            ->json("GET", $this->resourceUrl . "/" . $post->id)
            ->seeJson([
                "title" => $post->title,
                "summary" => $post->summary,
                "description" => $post->description,
                "category" => [
                    "id" => $post->category->id,
                    "name" => $post->category->name,
                ],
                "author" => [
                    "id" => $post->author->id,
                    "name" => $post->author->name,
                    "email" => $post->author->email,
                ],
            ])
            ->assertResponseStatus(200);
    }

    /** @test */
    public function it_throws_a_404_if_a_post_is_not_found()
    {
        $this
            ->json("GET", $this->resourceUrl . "/999999")
            ->seeJson([
                "error" => [
                    "message" => "Não foi possível encontrar um post com o identificador fornecido.",
                    "status_code" => 404
                ]
            ])
            ->assertResponseStatus(404);
    }

    /** @test */
    public function it_creates_a_new_post_given_valid_parameters()
    {
        $category = factory(Category::class)->create();

        $this
            ->actingAs($this->user)
            ->json("POST", $this->resourceUrl, [
                'title' => $this->fakeTitle,
                'summary' => $this->fakeSummary,
                'description' => $this->faker->paragraphs(3, true),
                'category_id' => $category->id,
            ])
            ->seeJson([
                "message" => "Post adicionado com sucesso."
            ])
            ->assertResponseStatus(201);

        $this->seeInDatabase("posts", [
            "title" => $this->fakeTitle,
            "summary" => $this->fakeSummary,
        ]);
    }

    /** @test */
    public function it_throws_a_422_if_a_new_post_doesn_not_contain_a_category()
    {
        $this
            ->actingAs($this->user)
            ->json("POST", $this->resourceUrl, [
                'title' => $this->fakeTitle,
                'summary' => $this->fakeSummary,
                'description' => $this->faker->paragraphs(3, true),
            ])
            ->seeJson([
                "category_id" => ["A categoria do post é obrigatória."]
            ])
            ->assertResponseStatus(422);
    }

    /** @test */
    public function it_throws_a_401_if_a_unauthorized_user_tries_to_store_a_new_post()
    {
        $this
            ->json("POST", $this->resourceUrl, [
                'title' => $this->fakeTitle,
                'summary' => $this->fakeSummary,
                'description' => $this->faker->paragraphs(3, true),
            ])
            ->seeJson([
                "message" => "Usuário não autenticado. Faça login e tente novamente."
            ])
            ->assertResponseStatus(401);
    }

    /** @test */
    public function it_publishes_a_post_if_the_published_date_is_null()
    {
        $category = factory(Category::class)->create();

        $this
            ->actingAs($this->user)
            ->json("POST", $this->resourceUrl, [
                'title' => $this->fakeTitle,
                'summary' => $this->fakeSummary,
                'description' => $this->faker->paragraphs(3, true),
                'category_id' => $category->id,
            ])
            ->seeJson([
                "message" => "Post adicionado com sucesso."
            ])
            ->assertResponseStatus(201);

        $this->seeInDatabase("posts", [
            "title" => $this->fakeTitle,
            "summary" => $this->fakeSummary,
            "publishing_date" => date("Y-m-d"),
        ]);
    }
    /** @test */
    public function it_relate_a_post_to_tags_if_they_are_provided()
    {
        $category = factory(Category::class)->create();
        $tags = factory(Tag::class, 5)->create();

        $this
            ->actingAs($this->user)
            ->json("POST", $this->resourceUrl, [
                'title' => $this->fakeTitle,
                'summary' => $this->fakeSummary,
                'description' => $this->faker->paragraphs(3, true),
                'category_id' => $category->id,
                'tags' => $tags->pluck("id"),
            ])
            ->seeJson([
                "message" => "Post adicionado com sucesso."
            ])
            ->assertResponseStatus(201);

        $post = Post::where([
            [ "title", "=", $this->fakeTitle ],
            [ "summary", "=", $this->fakeSummary ],
            [ "category_id", "=", $category->id ],
        ])->first();

        $randomTag = $tags[rand(0, 4)];

        $this->seeInDatabase("post_tag", [
            "post_id" => $post->id,
            "tag_id" => $randomTag->id
        ]);
    }

    /** @test */
    public function it_updates_a_post_given_valid_parameters()
    {
        $post = factory(Post::class)->create();
        $user = User::find($post->user_id);

        $this
            ->actingAs($user)
            ->json("PUT", $this->resourceUrl . "/" . $post->id, [
                'title' => "My new title!",
                'summary' => $post->summary,
                'description' => $post->description,
                'category_id' => $post->category_id,
            ])
            ->seeJson([
                "message" => "Post atualizado com sucesso."
            ])
            ->assertResponseStatus(200);

        $this->seeInDatabase("posts", [
            "id" => $post->id,
            "title" => "My new title!",
        ]);
    }



    /** @test */
    public function it_throws_a_403_if_a_user_tries_to_update_a_post_that_he_does_not_own()
    {
        $post = factory(Post::class)->create();

        $this
            ->actingAs($this->user)
            ->json("PUT", $this->resourceUrl . "/" . $post->id, [
                'title' => "My new title!",
                'summary' => $post->summary,
                'description' => $post->description,
                'category_id' => $post->category_id,
            ])
            ->seeJson([
                "message" => "Você não possui permissão para atualizar esse post."
            ])
            ->assertResponseStatus(403);

        $this->seeInDatabase("posts", [
            "id" => $post->id,
            "title" => $post->title,
        ]);
    }


    /** @test */
    public function it_throws_a_422_if_a_updating_post_request_fails_validation()
    {
        $post = factory(Post::class)->create();
        $user = User::find($post->user_id);

        $this
            ->actingAs($user)
            ->json("PUT", $this->resourceUrl . "/" . $post->id, [
                'title' => "My new title!",
                'summary' => $post->summary,
                'category_id' => $post->category_id,
            ])
            ->seeJson([
                "description" => ["A descrição do post é obrigatória."]
            ])
            ->assertResponseStatus(422);
    }

    /** @test */
    public function it_deletes_a_post()
    {
        $post = factory(Post::class)->create();
        $user = User::find($post->user_id);

        $this
            ->actingAs($user)
            ->json("DELETE", $this->resourceUrl . "/" . $post->id)
            ->seeJson([
                "message" => "Post removido com sucesso."
            ])
            ->assertResponseStatus(200);
    }

    /** @test */
    public function it_throws_a_404_if_a_post_to_delete_does_not_exist()
    {
        $this
            ->actingAs($this->user)
            ->json("DELETE", $this->resourceUrl . "/9898888")
            ->seeJson([
                "error" => [
                    "message" => "Não foi possível encontrar um post com o identificador fornecido.",
                    "status_code" => 404
                ]
            ])
            ->assertResponseStatus(404);
    }
}
