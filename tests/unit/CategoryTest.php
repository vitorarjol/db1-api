<?php

use App\Category;
use Laravel\Lumen\Testing\DatabaseTransactions;

class CategoryTest extends TestCase
{
    use DatabaseTransactions;
    protected $resourceUrl = "categories";

    /** @test */
    public function it_fetches_all_categories()
    {
        $category = factory(Category::class)->create();

        $this
            ->json("GET", $this->resourceUrl)
            ->seeJson([
                "name" => $category->name
            ])
            ->assertResponseStatus(200);
    }

    /** @test */
    public function it_fetches_a_single_category()
    {
        $category = factory(Category::class)->create();

        $this
            ->json("GET", $this->resourceUrl . "/" . $category->id)
            ->seeJson([
                "name" => $category->name
            ])
            ->assertResponseStatus(200);
    }

    /** @test */
    public function it_throws_a_404_if_a_category_is_not_found()
    {
        $this
            ->json("GET", $this->resourceUrl . "/999999")
            ->seeJson([
                "error" => [
                    "message" => "Não foi possível encontrar uma categoria com o identificador fornecido.",
                    "status_code" => 404
                ]
            ])
            ->assertResponseStatus(404);
    }

    /** @test */
    public function it_creates_a_new_category_given_valid_parameters()
    {
        $this
            ->json("POST", $this->resourceUrl, ["name" => "fooCategory"])
            ->seeJson([
                "message" => "Categoria adicionada com sucesso."
            ])
            ->assertResponseStatus(201);

        $this->seeInDatabase("categories", [
            "name" => "fooCategory"
        ]);
    }

    /** @test */
    public function it_throws_a_422_if_a_new_category_request_fails_validation()
    {
        $testCategory = factory(Category::class)->create();

        $this
            ->json("POST", $this->resourceUrl, ["name" => $testCategory->name])
            ->seeJson([
                "name" => ["Já existe uma categoria cadastrada com esse nome."]
            ])
            ->assertResponseStatus(422);
    }

    /** @test */
    public function it_updates_a_category_given_valid_parameters()
    {
        $category = factory(Category::class)->create();

        $this
            ->json("PUT", $this->resourceUrl . "/" . $category->id, ['name' => 'updated-category'])
            ->seeJson([
                "message" => "Categoria atualizada com sucesso."
            ])
            ->assertResponseStatus(200);
    }


    /** @test */
    public function it_throws_a_422_if_a_updating_category_request_fails_validation()
    {
        $category = factory(Category::class)->create();

        $this
            ->json("PUT", $this->resourceUrl . "/" . $category->id, ['name' => ''])
            ->seeJson([
                "name" => ["O nome da categoria é obrigatório."]
            ])
            ->assertResponseStatus(422);
    }

    /** @test */
    public function it_deletes_a_category()
    {
        $category = factory(Category::class)->create();

        $this
            ->json("DELETE", $this->resourceUrl . "/" . $category->id)
            ->seeJson([
                "message" => "Categoria removida com sucesso."
            ])
            ->assertResponseStatus(200);
    }

    /** @test */
    public function it_throws_a_404_if_a_category_to_delete_does_not_exist()
    {
        $this
            ->json("DELETE", $this->resourceUrl . "/9898888")
            ->seeJson([
                "error" => [
                    "message" => "Não foi possível encontrar uma categoria com o identificador fornecido.",
                    "status_code" => 404
                ]
            ])
            ->assertResponseStatus(404);
    }
}
