<?php

use App\Tag;
use Laravel\Lumen\Testing\DatabaseTransactions;

class TagTest extends TestCase
{
    use DatabaseTransactions;
    protected $resourceUrl = "tags";

    /** @test */
    public function it_fetches_all_tags()
    {
        $tag = factory(Tag::class)->create();

        $this
            ->json("GET", $this->resourceUrl)
            ->seeJson([
                "name" => $tag->name
            ])
            ->assertResponseStatus(200);
    }

    /** @test */
    public function it_fetches_a_single_tag()
    {
        $tag = factory(Tag::class)->create();

        $this
            ->json("GET", $this->resourceUrl . "/" . $tag->id)
            ->seeJson([
                "name" => $tag->name
            ])
            ->assertResponseStatus(200);
    }

    /** @test */
    public function it_throws_a_404_if_a_tag_is_not_found()
    {
        $this
            ->json("GET", $this->resourceUrl . "/999999")
            ->seeJson([
                "error" => [
                    "message" => "Não foi possível encontrar uma tag com o identificador fornecido.",
                    "status_code" => 404
                ]
            ])
            ->assertResponseStatus(404);
    }

    /** @test */
    public function it_creates_a_new_tag_given_valid_parameters()
    {
        $this
            ->json("POST", $this->resourceUrl, ["name" => "foo-tag"])
            ->seeJson([
                "message" => "Tag adicionada com sucesso."
            ])
            ->assertResponseStatus(201);

        $this->seeInDatabase("tags", [
            "name" => "foo-tag"
        ]);
    }

    /** @test */
    public function it_throws_a_422_if_a_new_tag_request_fails_validation()
    {
        $testTag = factory(Tag::class)->create();

        $this
            ->json("POST", $this->resourceUrl, ["name" => $testTag->name])
            ->seeJson([
                "name" => ["Já existe uma tag cadastrada com esse nome."]
            ])
            ->assertResponseStatus(422);
    }

    /** @test */
    public function it_updates_a_tag_given_valid_parameters()
    {
        $tag = factory(Tag::class)->create();

        $this
            ->json("PUT", $this->resourceUrl . "/" . $tag->id, ['name' => 'updated-foo-tag'])
            ->seeJson([
                "message" => "Tag atualizada com sucesso."
            ])
            ->assertResponseStatus(200);
    }


    /** @test */
    public function it_throws_a_422_if_a_updating_tag_request_fails_validation()
    {
        $tag = factory(Tag::class)->create();

        $this
            ->json("PUT", $this->resourceUrl . "/" . $tag->id, ['name' => ''])
            ->seeJson([
                "name" => ["O nome da tag é obrigatório."]
            ])
            ->assertResponseStatus(422);
    }

    /** @test */
    public function it_deletes_a_tag()
    {
        $tag = factory(Tag::class)->create();

        $this
            ->json("DELETE", $this->resourceUrl . "/" . $tag->id)
            ->seeJson([
                "message" => "Tag removida com sucesso."
            ])
            ->assertResponseStatus(200);
    }

    /** @test */
    public function it_throws_a_404_if_a_tag_to_delete_does_not_exist()
    {
        $this
            ->json("DELETE", $this->resourceUrl . "/9898888")
            ->seeJson([
                "error" => [
                    "message" => "Não foi possível encontrar uma tag com o identificador fornecido.",
                    "status_code" => 404
                ]
            ])
            ->assertResponseStatus(404);
    }
}
