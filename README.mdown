# db1-api

> Api desenvolvida com Lumen

## Sobre o projeto e algumas decisões arquiteturais

Nesse projeto, optei por desenvolver de maneira simples e intuitiva para que outros desenvolvedores possam se familiarizar rapidamente com o código desenvolvido. Dessa maneira, vocês notarão a não utilização de classes de serviço, repositórios, trocas na estrutura de pastas e itens nesse sentido.

A estrutura básica da API funciona da seguinte maneira:

- Uma requisição é enviada para a API
- É realizada a consulta no banco ou cache necessária
- Os dados passam por um layer que fica responsável por ser a interface da API. Dessa maneira, caso algum campo no banco mude, não teremos uma breaking chance na API porque esse layer cuida disso.
- Os dados são retornados para o usuário no formato JSON com os devidos códigos HTTP.
- Chamadas não autorizadas retornam um erro 401

## Passo a passo para utilização

``` bash
# instalação das dependências
composer install

# criar a entrada no Homestead.yaml

# Caso vocês queiram rodar com as mesmas configurações que utilizei, a url da aplicação é http://db1.api.dev. Aproveitei também para versionar o .env que utilizei nesse desenvolvimento.

# rodar migrations e seeders
php artisan migrate --seed

# executar o arquivo socket.js que se encontra na raíz do projeto para o broadcasting

# executar a SPA e consumir a api

```
